@extends('layout')

@section('content')
    <div>
        <article>
            <h1>Main title</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
                magna aliqua. Ullamcorper a lacus vestibulum sed arcu non odio. Sed augue lacus viverra vitae congue eu
                consequat. Tellus mauris a diam maecenas sed enim. Hendrerit gravida rutrum quisque non tellus orci. Mattis
                nunc
                sed blandit libero volutpat sed cras. Lacus suspendisse faucibus interdum posuere lorem ipsum dolor sit.
                Volutpat odio facilisis mauris sit amet massa vitae tortor. Venenatis lectus magna fringilla urna porttitor
                rhoncus dolor. Et tortor consequat id porta nibh venenatis cras sed felis. Enim facilisis gravida neque
                convallis a cras semper auctor neque. Ornare massa eget egestas purus viverra accumsan. Vulputate dignissim
                suspendisse in est. Mattis molestie a iaculis at erat pellentesque. Vel fringilla est ullamcorper eget nulla
                facilisi etiam dignissim diam.</p>

            <h2>Secondary title</h2>
            <p>Eleifend quam adipiscing vitae proin sagittis nisl rhoncus. Lorem mollis aliquam ut porttitor. Turpis massa
                sed
                elementum tempus egestas sed. Faucibus nisl tincidunt eget nullam non nisi est sit. Viverra aliquet eget sit
                amet tellus. Neque viverra justo nec ultrices dui sapien eget. A arcu cursus vitae congue mauris rhoncus.
                Odio
                pellentesque diam volutpat commodo sed. Et sollicitudin ac orci phasellus egestas tellus rutrum tellus
                pellentesque. Dui nunc mattis enim ut tellus. Eget dolor morbi non arcu risus quis varius. Id diam maecenas
                ultricies mi eget. Orci porta non pulvinar neque laoreet suspendisse. Ornare suspendisse sed nisi lacus sed
                viverra tellus in. Amet mauris commodo quis imperdiet massa tincidunt. Eu tincidunt tortor aliquam nulla
                facilisi cras fermentum odio. Sed adipiscing diam donec adipiscing.</p>

            <p>Quis risus sed vulputate odio ut enim blandit volutpat. Morbi enim nunc faucibus a pellentesque sit amet
                porttitor eget. Dolor morbi non arcu risus quis varius quam. Libero justo laoreet sit amet cursus sit amet.
                A
                lacus vestibulum sed arcu non odio. Pellentesque habitant morbi tristique senectus et netus et. Mi eget
                mauris
                pharetra et ultrices neque ornare aenean. Nisl condimentum id venenatis a condimentum. Fames ac turpis
                egestas
                sed tempus urna et pharetra. Augue eget arcu dictum varius duis at consectetur lorem donec. Varius morbi
                enim
                nunc faucibus a pellentesque sit amet. Odio ut sem nulla pharetra diam sit amet nisl suscipit. Tincidunt
                lobortis feugiat vivamus at augue eget arcu. At consectetur lorem donec massa sapien faucibus. Est ante in
                nibh
                mauris cursus mattis molestie. Fermentum posuere urna nec tincidunt. Nunc sed blandit libero volutpat sed
                cras
                ornare arcu dui.</p>

            <h2>Secondary title</h2>
            <p>Platea dictumst quisque sagittis purus sit amet volutpat consequat. Mauris ultrices eros in cursus turpis
                massa
                tincidunt dui. Viverra nam libero justo laoreet sit. Sed tempus urna et pharetra pharetra. Lorem mollis
                aliquam
                ut porttitor leo. Molestie a iaculis at erat pellentesque. Magna sit amet purus gravida quis blandit turpis
                cursus in. Sit amet consectetur adipiscing elit duis. Morbi non arcu risus quis. Venenatis cras sed felis
                eget
                velit aliquet sagittis. Tortor posuere ac ut consequat semper viverra.</p>

            <p>Tellus in hac habitasse platea. Tellus id interdum velit laoreet id. Feugiat sed lectus vestibulum mattis
                ullamcorper velit sed. Eu tincidunt tortor aliquam nulla facilisi cras fermentum odio eu. Sed velit
                dignissim
                sodales ut eu. Velit aliquet sagittis id consectetur purus ut faucibus pulvinar elementum. Amet aliquam id
                diam
                maecenas. Pulvinar etiam non quam lacus suspendisse faucibus interdum posuere. Pulvinar elementum integer
                enim
                neque volutpat ac. Pellentesque pulvinar pellentesque habitant morbi tristique senectus et netus. Vehicula
                ipsum
                a arcu cursus vitae congue. Sed ullamcorper morbi tincidunt ornare massa eget egestas purus viverra.
                Dignissim
                diam quis enim lobortis scelerisque fermentum dui. Id consectetur purus ut faucibus pulvinar elementum
                integer
                enim neque. Ut diam quam nulla porttitor massa.</p>
        </article>
    </div>
@endsection
