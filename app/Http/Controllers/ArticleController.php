<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Services\PageService;

class ArticleController extends Controller
{

    public function __construct(protected PageService $pageService)
    {
    }

    public function show($id)
    {

        $art = Article::where('slug', 'comment-entretenir-ces-cisaux-a-bois')->first();
        return view('article', ['article' => $art]);
    }
}
